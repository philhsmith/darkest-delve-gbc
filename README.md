# Darkest Delve Game Boy Color #

**Warning**: This is vaporware.

Many brave heroes have met their end in the Dungeon of Darkness!  Fight fearsome monsters,
dodge deadly traps and discover mighty artifacts as you plumb the eldritch depths.  Bring
the Dark Heart out into the sunlight to end the menace and win!

## The Game ##

Darkest Delve is a homebrew Game Boy Color game introducing real-time game play elements
into a classic rogue-like:
  * Randomly generated worlds
  * True death
  * Incomplete knowledge and identification
  * Spatial memory and limited vision

Unlike classic rogue-likes, Darkest Delve is not purely turn-based.  Instead action
proceeds in real time until the game is paused.  While the game is paused, a limited
turn-based mode called "Focus Time" can be activated.

## Focus Time ##

Focus Time allows the player to make a limited number of moves, called focus moves, while
the game is paused.  Focus moves will be performed in order automatically when the game is
unpaused.  Afterwards Focus Time cannot be used again for a little while (but the game may
still be paused).  The time to wait increases with the number of focus moves made.
