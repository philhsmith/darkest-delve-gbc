# vim: filetype=make

SHELL := /bin/bash

IMAGE := philhsmith/darkest-delve-gbc-dev:latest

UID=$(shell id -u)
GID=$(shell id -g)
DOCKER := docker run \
  --user '${UID}:${GID}' \
  --volume '${PWD}:/darkest-delve-gbc' \
  --workdir /darkest-delve-gbc \
  --interactive --tty \
  --name darkest-delve-gbc \
  --rm \
  ${IMAGE}

ifneq ($(shell which rgbasm),)
TOOL_SHELL := /bin/bash
else
TOOL_SHELL := ${DOCKER} /bin/bash
endif

ifneq ($(shell which VisualBoyAdvance),)
VISUALBOYADVANCE := VisualBoyAdvance
else
VISUALBOYADVANCE := visualboyadvance-m
endif

SRCS := $(wildcard *.m4)
DEPS := $(patsubst %.m4,build/%.d,$(SRCS))
ASMS := $(patsubst %.m4,build/%.asm,$(SRCS))
OBJS := $(patsubst %.m4,build/%.o,$(SRCS))

.PHONY: all
all: build/darkest-delve.gbc

.PHONY: test
test:

.PHONY: toolchain
toolchain:
	docker build -t ${IMAGE} .
	docker push ${IMAGE}

.PHONY: shell
shell:
	${DOCKER} /bin/bash

.PHONY: continuous-integration
continuous-integration: build/darkest-delve.gbc

.PHONY: clean
clean:
	-rm -rf build

.PHONY: run
run: build/darkest-delve.gbc
	${VISUALBOYADVANCE} $<

build:
	mkdir -p build

build/%.d: SHELL := ${TOOL_SHELL}
build/%.d: %.m4 | build
	sed -E \
	  -e 's|include\(\{(.*)\}\).*$$|$(@:.d=.asm): \1|p' \
	  -e 's|include "([^"]*)".*$$|$(@:.d=.asm): \1|p' \
	  -e d < $< > $@.partial
	mv $@.partial $@

include $(DEPS)

.SECONDARY: $(ASMS)
build/%.asm: SHELL := ${TOOL_SHELL}
build/%.asm: %.m4 | build
	m4 $< > $@.partial
	mv $@.partial $@

build/%.o: SHELL := ${TOOL_SHELL}
build/%.o: build/%.asm | build
	rgbasm -o $@ $<

build/darkest-delve.gbc: SHELL := ${TOOL_SHELL}
build/darkest-delve.gbc: $(OBJS) | build
	rgblink -o $@ $(OBJS)
	rgbfix -v $@
