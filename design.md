# Design #

**Warning**: Here thar be spoilers.

Part of the experience of developing a custom rom is designing for limited
hardware.  Here we note chosen trade-offs and the resulting data structures and
algorithms used.

## Hardware ##

The ROM is compatible with the original game boy (DMG), game boy color (GBC)
and super game boy (SGB).

It is built against the MBC3 memory bank controller equipped with:
  * 2 MB of bankable ROM
  * 32 KB of external, bankable RAM
  * a battery for persistent state
  * an RTC timer, for seeding RNG.

The ROM is presented as 16 KB banks.  The first rom bank is fixed and always
present, along side one selected from the other 127.

The external RAM is presented as 8 KB banks, of which one may be present at a
time.  In addition, 8 KB of internal work ram are available (the GBC-only paged
work ram is unused.)

This [excellent video][ultimate-gameboy-talk] explains the hardware in great
detail, as well as the included copy of the pandocs.

Beyond the visualboy advance emulator, I run development roms on a
[krikzz everdrive gb x7][everdrive] in a game boy color.

[ultimate-gameboy-talk]: https://www.youtube.com/watch?v=HyzD8pNlpwI
[everdrive]: https://stoneagegamer.com/flash/game-boy/system/everdrive-gb/x7/

## Game Design ##

Brogue is the foremost influence, followed by early top-down zeldas.
 
### Screen Flow ###

Upon entry a title sequence plays, leading to a title screen.  From here a menu
offers new game and load seed options.  New game samples a new seed by hashing
the RTC timer and proceeds to the first floor screen.  Load seed presents an
editable menu of previously saved seeds: after choosing one the first floor
screen is presented.  Editing a seed allows manual entry of e.g. 4 or 8 hex
digits.

Upon death a game over screen is presented, likewise winning presents a victory
screen.  Both contain a persistent score board and offers a menu to save or
retry the seed, or quit to the title screen.  Saving a seed leads to the save
seed screen, which is visually similar to the load seed screen but now prompts
the player to select a slot to overwrite.  Leaving the save seed screen returns
to the previous screen.

The floor screen is the central interface to game play.  It presents a top-down
view of the current floor as a grid of spaces.  Upon the grid the player,
monsters and other objects are depicted.  At the top and/or bottom of the
screen a fixed window is displayed containing a brief "heads up" summary of
game status.  The floor is larger than the view port, which scrolls as needed.

Pausing from the floor screen enters the pause screen.  The pause screen is
visually similar to the floor screen, and merely halts game play. Unpausing
returns to the floor screen.  From the pause screen the player may also instead
reach the focus screen.

The focus screen is visually similar to the floor screen, but the player may
schedule actions to perform upon unpause.  Currently buffered moves are
indicated visually.  The HUD summary is changed to reflect the remaining budget
of moves that may be buffered this way.  Leaving the focus screen returns to
the pause screen, with any buffered actions still presented visually.

The status screen is accessible from the floor screen.  It also halts game
play, and presents a full-screen view of the player's status, such as health,
score and equipped gear.  Leaving the status screen returns to the floor screen
and resumes game play.

The inventory screen is accessible from the status screen.  Here players can
examine their inventory to see an object's kind (e.g. "potion"), type ("red
potion" or, after discovery, "potion of healing"), known properties and marks
the player has made.  Items may be manipulated as well, such as by equipping or
unequipping gear, activating items that do not require aiming or marking.
Leaving the inventory screen returns to the status screen.

The discovery screen is accessible from the status screen.  It lists identified
consumable item types by kind ("potion"), allowing the player to guess by
elimination what an unidentified item might be.  It also allows the player to
mark object types ("red potion") and see previously made marks. Leaving the
discovery screen returns to the status screen.

TODO: An event log screen?

### Floors and the floor screen ###

Each floor is a 16x16 grid of "regions", which are themselves square 16x16
grids of spaces. Each floor is limited to 256 rooms.  Each room is generated
from a parameterized template, allowing ideas like "trove" or "lair" as well as
different kinds of open spaces and hallways.  There are 256 room templates,
though many are only small variations of each other.

Rooms are ultimately composed of spaces.  The player, items and medium-sized
monsters each occupy one space. In terms of game boy graphics each space is a
2x2 square of tiles (each tile being an 8x8 pixel grid).  Since the game boy
screen displays 20x18 tiles, we can render 10x9 spaces at a time.  With the HUD
display this to drops to 10x8 spaces.  The game boy's internal background map
is 32x32 tiles: one region of spaces.

Each space has a type, indicating the kind of terrain (stone, shallow or deep
water, grass, lava, etc).  The space type dictates what happens to the player,
monsters or other objects who enter, occupy or leave the space.  The number of
types of spaces is limited to 16.

On each floor is a down stair, leading to the floor below.  There are 16
floors.

## Software Design ##

From the point of view of software design, the procedurally generated world is
the most interesting aspect.  To make efficient use of memory, the world (once
generated) is not stored as a sparse grid of spaces.  Instead the generator,
driven by the seed, creates a compact description of each floor called the
floor plan.  The central piece of the floor plan is a list of instantiated room
templates called room layouts.  During generation, any necessary backtracking
or corrections to room layouts are performed before committing them to the
final floor plan.

Room layouts meet the requirement for a compact representation, but they create
another problem.  For a good play experience it is desirable to have smooth
rendering of the world as the player moves through it.  While most of that is
addressed by hardware, the game is required to correctly fill the background
tile map in a timely fashion.  Thus the game must be able to efficiently
consult the floor plan and produce appropriate rows and columns of tiles on
demand.  Call this the background tile rendering problem.  It can be decomposed
into two sub-problems: that of producing rows and columns of spaces ("space
rendering"), and translating those spaces into tiles ("tile translation").

Space rendering is achieved using regions as a basis for indexing.  Rooms
layouts in the room list are grouped by the regions containing their upper left
corners.  The region index then assigns each region the offset of the start of
its section.  In this way regions can quickly find the rooms they contain.  By
identifying regions with their spatial coordinates in the 16x16 floor grid, it
is possible to easily look up rooms in neighboring regions as well.  This
enables timely rendering by selecting just those rooms in the region and those
surrounding.

A rendered, sparse space map of a single region consumes 128 bytes (assuming
two spaces are encoded into a single byte), and so nine such regions consume
1152 bytes.  It is feasible to store the player's current region and up to
eight surrounding sparsely in work ram.  Space rendering then reduces to
rendering whole regions in a timely way when necessary with ample opportunity
to cache the result, and reading off space rows and columns for tile
translation.

To allow for large and interesting floors, room layouts are limited to 3 bytes.
This is achieved by realizing that the only decisions that must be recorded
about a room are those directly affecting floor generation.  Just as the floor
itself is procedurally generated from a seed, so the other deferred details of
a room may be (re)generated later during space rendering.  This requires that
space rendering a room is repeatable upon demand.

Pseudo random number generation is a key aspect of floor generation.  Since
most details of tile rendering are deferred until the player approaches their
containing regions, sampling entropy must be both deterministic and efficient.
To achieve determinism, not one but many seeds will be used.  Separate entropy
streams are created by sampling their seeds from some parent seed.  Each game
possesses a single "game seed", being the seed e.g. sampled from the RTC or
entered on the load seed screen.  The game seed is sampled to assign each floor
a "floor seed".  Each floor plan is generated from its floor seed.  Floor seeds
also are combined with room layout data to create "room seeds".  Room templates
sample from their dedicated room seed for entropy.

We are now in a position to describe our algorithms and data structures more
exactly.

### Floor Plan Generation ###

TODO: Region polyomino generation first?

TODO: Generation data structures are not rendering data structures

This is going to be pretty stream of mind while I decide what I want.  The
trick is finding a floor generation process that generally gives non-tree
floors without back-tracking.  It's ok to be sloppy: if a door opens onto a
brick wall that can be a fun (and infuriating) tactical surprise for the
player.  However most doors need to go somewhere, and floors cannot be
obviously tree-like.

The floor plan (rendering) data structures notably do not have any mention of
doors.  Instead they rely completely on room template identity for shape as
well as connectivity.  To make a connection between rooms we simply place two
layouts adjacently (overlapping?) such that the doors in each connect.

One can imagine, given a pair of distance doors that we wish to indirectly
connect, a carefully chosen set of room templates and a lookup table giving the
next templates one could place to ensure a connection.  That table could be
known at assembly time, but might be huge.  It also assumes that we track
unbound doors during floor generation, which is probably an unavoidable
nuisance.

A carefully chosen set of room templates might also offer alternatives for door
placement while otherwise preserving room shape.  Floor generation might
therefore be able to wave doors into or out of existence by twiddling template
ids.  This might cost too many shapes.

Doors might be implied by room adjacency.  Placement would need to be deferred
until space rendering, and so it must be quick.

You might get lucky and find an irregular tiling with 200+ distinct shapes.  Or
one with a dozen shapes that you might subtract space from to create variety.
Players might quickly learn if doors were only placed on outer edges.  Of
course there is nothing wrong with a two or three space black hallway
connecting rooms through the negative space.  That could make for some good
secret passages.

TODO: fast non-back-tracking polyomino tiling algorithms?

Rooms must connect directly if there is no separate notion of hallway.  To do
so we will need to identify not only the locations of doors, but their
orientation: the cardinal direction they open out to.  Two rooms may only
connect if they have a pair of adjacent doors opening onto each other.

When considering one room and where to place its neighbors, we will need to
know the locations and orientations of its doors.  One can imagine tables
taking template ids to lists of door orientations and locations relative to
room origin.  These lists might be sorted by some standard ordering of
orientation and paired with an indexing byte (assuming three doors per
orientation will suffice) to enable quick access by orientation.

We will have similar need to understand where to place neighboring rooms with
which templates to align connecting doors.  Separate tables for each door
orientation can enumerate template ids and offsets of the room bounding boxes
relative to the door.

These indexes (TODO: need names) work together to enable us to enumerate all
potential neighbors of a given room.  It remains to maintain a frontier of
doors needing neighbors, and to choose a way to select doors from it to expand.
And what do to when no neighboring room can be found.  Probably nothing: brick
over it and move on.

Greedily expanding random doors in the frontier will give us a tree of rooms.
We would like loops as well.  In a graph without edge constraints it would be
trivial to do this by adding cross-links.  However we must fit a corrodor of
heterogenously-shaped rooms between two given doors.

Here's a thought.  Instead of a tree the floor becomes a series-parallel graph.
We choose a pair of initial, distant terminals and build by subdivision.  A
frontier of terminal pairs is maintained.  When considering a pair, changes to
door count must be preserved on both ends, resulting in the introduction or
elimination of terminal pairs.  Thus we are free to add a hallway to one
terminal, but adding a (binary) fork to one demands a similar fork to the other
with the result that the current terminal pair is dropped from the frontier and
two new ones are added.  Old terminals are eliminated when their doors meet or
they split.  New ones are introduced by splits.

If we don't care to balance changes to door counts at each terminal,
differences could be thrown to one side as a new corrodor to build towards.

If rooms have a max size in each dimension (and they do) and we are careful to
choose enough variety of room shapes, we may be able to guarantee that we
always have a room shape to connect a pair of nearby doors.

The advantage of a series-parallel algorithm is we avoid the naive, quadratic
consideration of door pairs for creating cross links.  Each frontier door has
one neighbor in mind.  Without embracing the idea completely, we could still
borrow the notion of introduced loops: when a fork is created its pairing join
could be eagerly placed and worked towards.

The reason arbitrary door pairs can't be matched is the floor plan must be
planar.  Orientation can make it awkward as well.  We might rely on tile
distance as a heuristic for choosing frontier doors to attempt to match.  Since
orientation determines where the next step on the path must be, we would
measure distances not from the doors but one step beyond.

Since doors have absolute coordinates, checking the manhattan distance between
any pair is simple.  Ranking neighbors by distance is a separate problem, which
is general enough to surely be well understood.

So a stochastic algorithm begins to emerge.  Grow the graph randomly from a
single initial room by expanding a frontier of unconnected doors.  Two forces
influence the choice of room template: apreference to "be big", to push in new
directions and expand the floor's bounding box, and a preference to close
loops, to shrink the distance between already-close frontier doors.  If we can
close a loop, we do.  Otherwise we flip a weighted coin based on closest-door
distance, and either work towards a close door or push out.

### Seeds ###

A seed is 4 bytes long. TODO: Or is it 2?

### Coordinate Pairs ###

We have need to refer to points in 16x16 grids.  Points conveniently fit in a
byte, and are decomposed as two 4-bit coordinates:

```
+---------------+
|   Y   |   X   |
+---------------+
```

These bytes may also of course be interpreted as unsigned 8 bit identifiers.
Assuming the origin in the upper left, this encoding sends identifier 0 to the
upper left with successive identifiers going to the right, row by row down as
the English would read a book.  Region identifiers are ordered by their
unsigned 8 bit integer values.

### Room Identifiers ###

Each room within a floor is assigned a 1 byte opaque identifier.

### Template Identifiers ###

Each room template is assigned a 1 byte opaque identifier.

### Room States ###

Every room layout is given 1 byte of opaque, mutable state, which the room's
template initializes and uses to drive a state machine.

### Room Origins ###

The coordinate pair of the upper left tile of a room's bounding box.  It is
identified with the location of the room itself.

### Room Layouts ###

Room layouts are composed of three 256 byte ladder arrays, collectively called
the room layout lists:
  * The room location list
  * The room template list
  * The room state list

As hinted by the word "ladder", each are indexed by the shared domain of room
identifiers.  That is, the location of room `$05` is found at offset `$05` in
the location list, while its template id is found at offset `$05` in the
template list.

Thus there is no single contiguous memory area that corrosponds to a single
room layout.  It should be noted that the game boy has no hierarchical memory
caches; the argument for spatial locality in memory layout is much weaker than
for modern processors.  Thus ladder arrays sacrifice little while making
pointer arithmetic easier.

The order of room layouts within these lists is constrained.  In each list the
entries for rooms whose origins share a region must appear in a single
contiguous run.  Moreover, these runs appear in region identifier order: the
rooms in the run for region `$03` must appear before those of region `$04`.
Room layouts within the same region run do not have any ordering constraints.
This ordering does not contradict the fact that these lists are indexed by room
identifiers: room layout order within these lists is normative and determines
room identifier assignment.

### The Room Location List ###

The room location list is a (256 byte) page-aligned 256 byte array of points,
mapping rooms to the location of their origin within its enclosing region.

### The Room Template List ###

The room template list is a page-aligned 256 byte array of template ids,
mapping rooms to their room templates.

### The Room State List ###

The room state list is a page-aligned 256 byte array of room states, mapping
rooms to their mutable state.

### Region Identifiers ###

Region identifiers are 1-byte points in the floor's 16x16 region grid.

### Region Index ###

The region index is a page-aligned 256 byte array of room identifiers, mapping
region identifiers to the room identifiers.  Recall that the room layout lists
present rooms grouped by region, in increasing region id order.  The region
index sends each region id to the identifier of the first room in its group.

Concretely, each region identifier is sent to the sum of the sizes of the
groups of all preceeding regions.

Thus region id `$00` is sent to room id `$00`, and if a region's group is
empty, its identifier is sent to the same room identifier as the preceeding
region.  To identify the rooms in a region's group, we retrieve the room ids
for it and the next region id.  The resulting start-inclusive end-exclusive
room id range identifies the rooms exactly.

This design creates one problem.  We can't determine the end of the last
region's group, since there is no following region to give us an ending room
id.  This is resolved by noting that the first region id is sent to room id
`$00` by construction.  Instead of storing this literal `$00` the first byte of
the region index records the number of rooms (minus 1) and so identifies the
(inclusive now) end of the last region's group.

### Floor Plan ###

A floor plan is composed of
* a floor seed
* three room layout lists
* a region index

It occupies 1024 + 4 (2?) bytes.

TODO: doors, connectivity
