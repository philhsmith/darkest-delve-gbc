; vim: set fenc=utf-8 ff=unix sts=2 sw=2 et ft=asm :
changequote()changequote({,})changecom({;})dnl
include({sections.m4i})dnl

include "hardware.inc"

; Interrupt vectors

section_reset_00()
  jp HEADER

section_reset_08()
  jp HEADER

section_reset_10()
  jp HEADER

section_reset_18()
  jp HEADER

section_reset_20()
  jp HEADER

section_reset_28()
  jp HEADER

section_reset_30()
  jp HEADER

section_reset_38()
  jp HEADER

section_irq_vblank()
  reti
  
section_irq_lcd()
  reti

section_irq_timer()
  reti

section_irq_serial()
  reti

section_irq_joypad()
  reti

; Cartridge header

section_header()
HEADER:
  nop
  jp MAIN

  NINTENDO_LOGO

  DB "DARKST DELV"              ; Game title, 11 ascii, pad with $00
  DB "    "                     ; Manufacturer code, 4 ascii, leave blank
  DB $00 ; TODO: $80                      ; Color Game Boy compatibility flag (DMG and GBC)
  DB $00, $00                   ; New licensee code
  DB $03                        ; Super Game Boy compatibility flag (SGB)
  DB $10                        ; Cartridge type (MBC3, timer, ram, battery)
  DB $06                        ; ROM size (2 MB, 128 banks)
  DB $03                        ; RAM size (32 KB, 4 banks)
  DB $01                        ; Destination code (outside Japan)
  DB $33                        ; Old licensee code
  DB $00                        ; Mask ROM version (set by rgbfix)
  DB $00                        ; Header checksum (set by rgbfix)
  DW $00                        ; Global checksum (set by rgbfix)

; Game code

section_main()
MAIN:
  di                            ; disable interrupts
  ld sp, $FFFE                  ; set the stack to $FFFE
  call WAIT_VBLANK              ; wait for v-blank

  ld a, 0
  ldh [rLCDC], a                ; turn off LCD

  call CLEAR_MAP                ; clear the BG map
  call LOAD_TILES               ; load up our tiles
  call LOAD_MAP                 ; load up our map

  ld a, %11100100               ; load a normal palette up 11 10 01 00 - dark->light
  ldh [rBGP], a                 ; load the palette
  
  ld a, %10010001
  ldh [rLCDC], a                ; turn on the LCD, BG, etc

.loop:
  halt
  jp .loop

section_common()

WAIT_VBLANK:
  ; halt                        ; TODO: low power mode
  ldh a, [rLY]                  ; get current scanline
  cp $91                        ; Are we in v-blank yet?
  jr nz, WAIT_VBLANK            ; if A-91 != 0 then loop
  ret                           ; done
  
CLEAR_MAP:
  ld hl, _SCRN0                 ; loads the address of the bg map ($9800) into HL
  ld bc, 32 * 32
.loop:
  ld a, 0                       ; tile 0 is a blank
  ld [hl+], a
  dec bc
  ld a, b
  or c
  jr nz, .loop
  ret

LOAD_TILES:
  ld hl, ASCII_TILES
  ld de, _VRAM
  ld bc, 128 * 16               ; 128 ascii tiles, each takes 16 bytes
.loop:
  ld a, [hl+]
  ld [de], a
  inc de
  dec bc
  ld a, b
  or c
  jr nz, .loop
  ret

LOAD_MAP:
  ld hl, HELLO_MAP
  ld de, _SCRN0
  ld c, 13
.loop:
  ld a, [hl+]
  ld [de], a
  inc de
  dec c
  jr nz, .loop
  ret

;********************************************************************
; Pattern table

section_tiles()

ASCII_TILES:
  INCBIN "ascii.chr"

;************************************************************
;* tile map

section_map()

HELLO_MAP:
  DB "Hello, World!"
